<?php

$sections = [
  'ESN AUA' => 'aua.esngreece.gr',
  'ESN AUTH' => 'auth.esngreece.gr',
  'ESN DUTH' => 'duth.esngreece.gr',
  'ESN HMU' => 'hmu.esngreece.gr',
  'ESN IHU' => 'ihu.esngreece.gr',
  'ESN Ioannina' => 'ioannina.esngreece.gr',
  'ESN Panteion' => 'panteion.esngreece.gr',
  'ESN TUC' => 'tuc.esngreece.gr',
  'ESN Unipi' => 'unipi.esngreece.gr',
  'ESN UOC' => 'uoc.esngreece.gr',
  'ESN UOM' => 'uom.esngreece.gr',
  'ESN UOPA' => 'uopa.esngreece.gr',
  'ESN UTH' => 'uth.esngreece.gr',
  'ESN West Attica' => 'westattica.esngreece.gr',
  'ESN Western Macedonia' => 'teiwm.esngreece.gr'
];

$positions = [
  'Webmaster' => 'webmaster',
  'President' => 'president',
  'Vice President' => 'vicepresident',
  'Treasurer' => 'treasurer',
  'Communication' => 'communication',
  'Local Representative' => 'lr',
];


