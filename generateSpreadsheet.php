<?php

require 'vendor/autoload.php';
require 'configuration.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

$columns = ['A', 'B', 'C', 'D', 'E'];

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();


//Set headers
$sheet
    ->setCellValue('A1', 'First Name [Required]') //A
    ->setCellValue('B1', 'Last Name [Required]') //B
    ->setCellValue('C1', 'Email Address [Required]') //C
    ->setCellValue('D1', 'Password [Required]') //D
    ->setCellValue('E1', 'Org Unit Path [Required]'); //E

$row = 2;
foreach ($sections as $section => $domain) {
    foreach ($positions as $position => $username) {
        $sheet->setCellValue('A'.$row, $position)
            ->setCellValue('B'.$row, $section)
            ->setCellValue('C'.$row, $username . '@' . $domain)
            ->setCellValue('D'.$row, 1234567890)
            ->setCellValue('E'.$row, '/Sections/' . $section);
        $row++;
    }
}


$writer = new Csv($spreadsheet);
$writer->save('test.csv');
